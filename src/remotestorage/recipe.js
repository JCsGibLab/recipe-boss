export default {
    name: 'recipes',
    builder: function(privateClient) {
        privateClient.declareType('recipe', {
            type: 'object',
            properties: {
                title: {type: 'string'},
                source: {type: 'string'},
                description: {type: 'string'},
                ingredients: {type: 'string'},
                directions: {type: 'string'},
                notes: {type: 'string'},
                prep_time: {type: 'number'},
                total_time: {type: 'number'},
                image: {type: 'string'},
                rating: {type: 'number'},
            },
            required: [
                'title',
            ],
        });

        return {
            exports: {
                init: () => {
                    privateClient.cache('');
                },

                on: privateClient.on,

                add: (recipe) => privateClient.storeObject('recipe', recipe.id, recipe),

                find: privateClient.getObject.bind(privateClient),

                delete: privateClient.remove.bind(privateClient),

                list: () => privateClient.getAll(''),
            },
        };
    },
};
