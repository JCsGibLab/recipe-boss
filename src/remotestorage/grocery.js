export default {
    name: 'groceries',
    builder: function(privateClient) {
        privateClient.declareType('grocery', {
            type: 'object',
            properties: {
                name: {type: 'string'},
                notes: {type: 'string'},
                categoryId: {type: 'string'},
                quantity: {type: 'number'},
                unit: {type: 'string'},
            },
            required: [
                'name',
            ],
        });

        return {
            exports: {
                init: () => {
                    privateClient.cache('');
                },

                on: privateClient.on,

                add: (grocery) => privateClient.storeObject('grocery', grocery.id, grocery),

                find: privateClient.getObject.bind(privateClient),

                delete: privateClient.remove.bind(privateClient),

                list: () => privateClient.getAll(''),
            },
        };
    },
};
