#!/bin/bash

npm run build-app

# Make the css/js links relative
sed -i 's#="/#="#g' ./dist/index.html
sed -i 's#=/#=#g' ./dist/index.html

# Apply app specific overi
sed -i 's/<link href=vanilla.css rel=stylesheet>/<link href=vanilla.css rel=stylesheet><link href=overrides.css rel=stylesheet>/g' ./dist/index.html

rm -rf ./ubuntu-touch-app/www/
mkdir -p ./ubuntu-touch-app/www/
cp -r ./dist/* ./ubuntu-touch-app/www/
rm ./ubuntu-touch-app/www/manifest.json
